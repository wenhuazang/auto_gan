\documentclass{article}

\usepackage{graphicx} % more modern
\usepackage{subfigure}
\usepackage{wrapfig}
\usepackage{xspace}
\usepackage{caption}

\usepackage{rotating}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{enumitem}
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usepackage{fullpage}
\usepackage{authblk}

% other packages
\usepackage{Definitions}
\usepackage{color}
\newcommand{\Bo}[1]{{\color{blue} [Bo: #1]}}

\renewcommand{\leq}{\leqslant}
\renewcommand{\le}{\leqslant}
\renewcommand{\geq}{\geqslant}
\renewcommand{\ge}{\geqslant}
\newcommand{\bigchi}{\mbox{\Large$\chi$}}

\title{\huge Variational Auto $f$-GAN-coder: \\
A Unified View for Generative Adversarial Networks and Variational Auto-encoder}

% \author{ Bo Dai}

\begin{document}
\maketitle

% \begin{abstract}
% \end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminary}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\setlength{\abovedisplayskip}{3pt}
\setlength{\abovedisplayshortskip}{1pt}
\setlength{\belowdisplayskip}{3pt}
\setlength{\belowdisplayshortskip}{1pt}
\setlength{\jot}{2pt}

\setlength{\floatsep}{2ex}
\setlength{\textfloatsep}{2ex}

At first glance, the generative adversarial networks~(GAN) and variational auto-encoder~(VAE) are purely separated two completitors for learning generative models. However, in this work, we will reveal the connections between these two models. Before we show the equivalence between these two models, we first introduce the $f$-divergence. 

{\bf $f$-divergence} Consider two distribution $\PP$ and $\QQ$, both assumed to be absolutely continuous w.r.t. Lebesgue measure $\mu$ with densities $p$ and $q$ on some compact domain $\Xcal$. The clas of $f$-divergence or Ali-Silvey divergence can be understood as the ``distances'' in the form 
\begin{eqnarray*}
D_\phi(\PP||\QQ) := \int p(x) \phi\rbr{\frac{q(x)}{p(x)}}dx
\end{eqnarray*}
where $\phi: \RR_+\rightarrow \RR \cup\{+\infty\}$ is a continuous convex function, satisfying $\phi(1) = 0$. Different choice of $\phi$ result in different divergences. $KL$-divergence, Hellinger distance, and Jensen-Shannon divergence are special cases of $f$-divergence.

{\bf Variational Representation:} Since $\phi$ is a convex function, denote the Fenchel's duality of $\phi$ as $\phi^*$, we have
\begin{eqnarray*}
\phi(u) = \sup_{v\in \RR}\rbr{\langle u, v\rangle - \phi^*(v)}. 
\end{eqnarray*}
Therefore, we can represent the $\phi$-divergence as 
\begin{eqnarray*}
D_\phi(\PP||\QQ) = \int p(x)\sup_{v(x)}\rbr{v(x)\frac{q(x)}{p(x)} - \phi^*(v(x))}dx \ge \sup_{v(\cdot)\in \Fcal}\rbr{\int v(x) d\QQ - \int \phi^*(v(x)) d\PP}
\end{eqnarray*}
From~\cite{BoyVan04}, it can be shown that the supremum will be achieved at function $v$ such that $v\in \partial \phi\rbr{\frac{q}{p}}$ for arbitrary $x$. If the $\partial \phi\rbr{\frac{q}{p}}(\cdot) \in \Fcal$, then, the equality holds. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Viewing GAN and VAE as optimizing special $f$-divergences}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

With the knowledge about $f$-divergence and its variational representation, the VAE and GAN can be viewed as optimizing special $f$-divergences, $KL$-divergence and Jensen-Shannon divergence, respectively, with different surrogate functions. Thoughout this paper, we denote $\hat p(x)$ as the empirical distribution, the generative model to be learned denoted as $p(x) = \int p(x, h)dh$.

\subsection{VAE is optimizing $KL$-divergence}

In variational auto-encoder, the utlimate objective in optimization is the marginal likelihood
\begin{eqnarray}\label{eq:VAE_obj}
\max_{p(x,h)}\EE_{\hat p(x)}\sbr{\log \int p(x, h)dh}.
\end{eqnarray}
In fact, it is equivalent to minimize the KL-divergence, \ie, 
\begin{eqnarray}\label{eq:KL_likelihood}
\min_{p(x)} D_{KL}(\hat p(x) ||p(x)) := \EE_{\hat p(x)}\sbr{\log \hat p(x) - \log p(x)} \propto \min_{p(x)}\EE_{\hat p(x)}\sbr{-\log \int p(x, h)dh}.
\end{eqnarray}
which is exactly maximizing the marginal likelihood. However, due to its intractablility, in VAE, we find a surrogate function which is the upper bound of the objective by convexity, \ie, Jensen's inequality,
\begin{eqnarray}\label{eq:VAE_surrogate}
\min_{p(x)}\EE_{\hat p(x)}\sbr{-\log \int p(x, h)dh} \le \max_{p(x, h), q(h|x)\in \Pcal}\EE_{\hat p(x)}\EE_{q(h|x)}\sbr{-\log p(x, h) + \log q(h|x)}
\end{eqnarray}
As we discussed, if we set $\phi(u) = u\log u$ in $f$-divergence, we achieve the $KL$-divergence. Therefore, the VAE is optimizing the \emph{$KL$-divergence} via its \emph{upper} bound as surrogate.

\noindent{\bf Remark:} The equality can be achieved if $\Pcal$ contains the true posterior, \ie, $p(h|x) = \frac{p(x, h)}{p(x)}\in \Pcal$. Therefore, we have 
\begin{eqnarray*}
\int p(h|x) \log \frac{p(x, h)}{p(h|x)}dh = \int p(h|x) \log p(x)dh = \log p(x) = \log \int p(x, h)dh.
\end{eqnarray*}


\subsection{GAN is optimizing Jensen-Shannon divergence}

Introduced by~\cite{GooPouMirXuetal14,NowCseTom16}, the original objective of GAN can be understood as the variational form of Jensen-Shannon divergence~(with extra contant term), which can be obtained by setting $\phi(u) = u\log u - (u+1)\log \frac{1+u}{2}$. Specifically, recall the model in GAN is $p(x)$ which is parametrized as 
\begin{eqnarray*}
x|h \sim \delta_{\alpha(h)}, \quad h\sim p(h),
\end{eqnarray*}
where $\alpha(\cdot)$ denotes some differentiable function and $p(h)$ is some reference distribution which can be easily sampled from. We can treat $h$ as the latent variable, and thus, the model $p(x) = \int p(x, h)dh = \int \delta(x - f(h))p(h)dh$. Plug such model and $\phi$ into the $f$-divergence and its variational representation, we have 
%
\begin{eqnarray}
\min_{p_\alpha(x, h)} D_{JS}(\hat p(x) || p_\alpha(x)) &:=&\frac{1}{2} \int \hat p(x) \log\frac{2\hat p(x)}{\hat p(x) + p_\alpha(x)} + p_\alpha(x) \log\frac{2p_\alpha(x)}{\hat p(x) + p_\alpha(x)} dx \\
&\ge& \frac{1}{2}\log(4) + \frac{1}{2}\min_{p_\alpha(x, h)}\max_{v(\cdot)<0, v(\cdot)\in\Fcal}\EE_{\hat p(x)}\sbr{v(x)} + \EE_{p(h), p_\alpha(x|h)}\sbr{\log\rbr{1 - \exp(v(x))}}\\
&=&\frac{1}{2}\log(4) + \frac{1}{2}\min_{p_\alpha(x, h)}\max_{g(\cdot)<1, \log g(\cdot)\in\Fcal}\EE_{\hat p(x)}\sbr{\log g(x)} + \EE_{p(h), p_\alpha(x|h)}\sbr{\log\rbr{1 - g(x)}}
\end{eqnarray}
where the last equation because $g(x) = \exp(v(x))$. Therefore, the GAN is optimizing \emph{Jensen-Shannon divergence} via its \emph{lower} bound as the surrogate.

\noindent{\bf Remark:} Denote the optimal solution as $p^*(x)$, then, if there exists $g(x)\in \partial \phi(\hat p/ p^*)|_x\in \Fcal$ for arbitrary $x$, the equality holds.

% \noindent{\bf Remark:} With the understanding of VAE and GAN in terms of optimizing a special $f$-divergence, it is straightforward to generalize the learning of VAE and GAN by optimizing the other $f$-divergence, resulting $f$-VAE and $f$-GAN~\cite{NowCseTom16} 


% By the Fenchel's duality, we can rewrite the KL-divergence as 
% \begin{eqnarray}\label{eq:KL_dual}
% \min_{p(x)}KL(\hat p(x)||p(x)) &=& \min_{p(x)}\sup_{\phi<0} \sbr{\EE_{p(x)}\sbr{\phi(x)} - \EE_{\hat p(x)}\sbr{\rbr{-1-\log(-\phi)} }}\\
% &\propto& \min_{p(x)}\sup_{\phi<0} \sbr{\EE_{p(x)}\sbr{\phi(x)} + \EE_{\hat p(x)}\sbr{\log(-\phi) }}
% \end{eqnarray}
% By viewing the $\phi$ be the discriminator, this is a special variant of the GAN~\cite{GooPouMirXuetal14}, which we named as KL-GAN. In fact, the GAN objective can be derived as the Fenchel duality of Jensen-Shannon divergence between $\hat p(x)$ and $p(x)$. Consider the KL-divergence and Jensen-Shannon divergence are both the special form of $f$-divergence, the GAN can be generalized to learning generative model $p(x)$ by matching $f$-divergence~\cite{NowCseTom16}.

% \Bo{Therefore, we can directly apply stochastic gradient to the saddle point objective. }

% By the transformation, we can see that the VAE and KL-GAN are the same in terms of optimization objective. The only difference is how to lower bound the marginal likelihood. With such understanding, we have three benefits
% \begin{itemize}
% \item[1] Using KL-GAN for graphical model learning. 
% \item[1] Using KL-GAN for variational inference.
% \item[2] Using VAE to improve GAN.
% \end{itemize}

% The first one is obvious by considering the equivalence between marginal loglikelihood and KL-divergence. We mainly focus on the latter two. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Comparison between VAE and GAN}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Based on the view in optimizing special $f$-divergence, we connect the VAE and GAN into one unified framework: both of these two models are learning the generative model with latent variable via matching to the empirical dataset. The major differences lies in three aspects:
%
\begin{itemize}
\item[1.] They use different $\phi$ in $f$-divergence, so that, they have different divergences as the objective. Specifically, the GAN is trying to minimize the \emph{Jensen-Shannon divergence} while the VAE is trying to minimize the \emph{$KL$-divergence} between the empirical marginal distribution to the generative model with latent variables.
\item[2.] They use different representations to obtain different surrogates of the objective functions. Specifically, in the GAN model, the \emph{lower} bound of the Jensen-Shannon divergence is obtained by applying the Fenchel's duality, while in the VAE model, the \emph{upper} bound of the $KL$-divergence is obtained by applying the convexity.  
\item[3.] Because the different surrogates of the objective functions, they have different components in the learning procedure. Specifically, the VAE incorporates the \emph{posterior model} $q(h|x)$ naturally with the \emph{forward model} $p(x|h)$, while the GAN only trained the \emph{forward model} $p(x|h)$ with the \emph{dual function} as the ``discriminator''.
\end{itemize}
%
Therefore, we have clear understanding about the pros and cons for each methods:
%
\begin{itemize}
\item[1.] {\bf Generality:} Since the Fenchels' dual applies to all convex functions, the GAN model can be generalized to arbitrary $f$-divergence, which results $f$-GAN~\cite{NowCseTom16}. In~\cite{NowCseTom16}, they claimed the performances of different $f$-divergences matters in different tasks. While for VAE, the upper bound is obtained by the convexity of $-\log(\cdot)$ which is particularly to $KL$-divergence.
\item[2.] {\bf Robustness:} VAE uses the upper bound, while GAN uses the lower bound to be the surrogate for optimization tractability. Consider the ultimate objective is \emph{minimizing} the corresponding $f$-divergences, under the general practical setting that the model assumption are not correct and there are some approximation error, \ie, $\frac{p(x, h)}{p(x)}\notin \Pcal$ and $\partial \phi(\hat p/ p^*)\notin \Fcal$, the upper bound always a good criterion for minimization, while the lower bound may become vanity. 

\Bo{may add some experiments to justify this claim. In fact, I have implemented the GAN with linear model in the google drive. I tested the performance preliminarily. It follows this conjecture. The GAN is worse than VAE.}
\item[3.] {\bf Inference Efficiency:} In VAE, the posterior model, \ie, $q(h|x)$, is introduced naturally as a part of the upper bound and learned together with the generative model. While, in GAN, only the generative model is learned.
\end{itemize}
%
In such sense, if the $f$-divergence is chosen to be $KL$-divergence, it is better to use the VAE with the upper bound comparing to GAN with the lower bound. On the other hand, if other divergences are chosen, then the Fenchel's duality should be applied, which results a saddle point optimization, similar in GAN. If the posterior model is needed, which is true in most cases, the VAE should be chosen.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Variational auto $f$-GAN-coder}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


As we introduced in previous section, the model in GAN and other $f$-GAN is forward marginal distribution $p(x)$, which does not explicit included the posterior model $q(h|x)$. Although the posterior model is incorporated in VAE, the surrogate of the objective in VAE is only valid for $KL$-divergence. Can we incorporate the $q(h|x)$ into $f$-GAN? Can we generalize the learning of VAE to other divergences? In this section, we provide an answer for both these two questions by proposing a new model, named as variational auto $f$-GAN-coder~(VAF), sharing both the benefits of VAE and GAN. 

% \Bo{In variational inference we have model $p(x) = \int p(x, h)dh = \int p(x|h)p(h)dh$, the target is to infer the posteior $q(h|x)$. The proposed model can be viewed as variational inference.}


\subsection{Introduce posterior model to $f$-GAN}

Recall the total probability theorem
$$
p(x, h) = p(x|h)p(h) = p(h|x)p(x),
$$
applying the equality, we have the following equation,
\begin{eqnarray}
\EE_{p(x, h)}\sbr{f(x, h)} = \EE_{p(x), p(h|x), p(x'|h)}\sbr{f(x', h)}.
\end{eqnarray}

The proof is trivial
\begin{proof}
\begin{eqnarray*}
\EE_{p(x, h)}\sbr{f(x, h)} &=& \int p(x, h) f(x, h)dhdx\\
&=& \int p(x, h)\phi(x, h)\rbr{f p(x'|h)dx'}dxdh\\
&=& \int p(h) p(x|h)p(x'|h) f(x, h)dx'dxdh\\
&=& \int p(h) p(x|h)p(x'|h) f(x', h)dx'dxdh\\
&=& \int p(x, h)p(x'|h) f(x', h)dx'dxdh\\
&=& \int p(x) \int p(h|x) \int p(x'|h) f(x',h) dx'dh dx\\
&=& \EE_{p(x), p(h|x), p(x'|h)}\sbr{ f(x', h)}
\end{eqnarray*} 
\end{proof}

Plug this into the variational representation of $f$-GAN with generating function $\phi$, we have
\begin{eqnarray}\label{eq:VAE-GAN}
\min_{p(x'|h), p(h|x)}\max_{v(\cdot)\in \Fcal} {\EE_{\hat p(x), p(h|x), p(x'|h)}\sbr{v(x')} - \EE_{\hat p(x)}\sbr{\phi^*(v(x)) }}
\end{eqnarray}

% \Bo{Add more details here. The decomposition is valid on optimal solution, \ie, $p(x) = \hat p(x)$.}

% \subsection{Reinterpret with KL-GAN}

Despite the mathematical derivation in introducing the posterior model, we reinterpret the proposed variational auto $f$-GAN-coder in the GAN model view. Denote the $p(x'|x) = \int p(x'|h)p(h|x)dh$ and $p(x') = \int \hat p(x)p(x'|x)dx$, we rewrite the objective as
\begin{eqnarray*}
&&{\EE_{\hat p(x), p(h|x), p(x'|h)}\sbr{v(x')} - \EE_{\hat p(x)}\sbr{\phi^*(v(x)) }}\\
&=&-\int v(x')\rbr{\int p(x'|h)p(h|x)dh}\hat p(x)dx'dx + \int \phi^*(v(x))\hat p(x)dx\\
&=& -\int v(x')p(x'|x)\hat p(x)dx'dx + \int \phi^*(v(x))\hat p(x)dx\\
&=& -\int v(x')p(x')dx' + \int \phi^*(v(x))\hat p(x)dx\\
&=&\EE_{p(x')}\sbr{v(x')} - \EE_{\hat p(x)}\sbr{\phi^*(v(x)) }.
\end{eqnarray*}
%
From this perspective, we can view the variational auto $f$-GAN-coder is a variant of $f$-GAN with special structure in the generative model, $p(x)$. Specifically, rather than traditional $f$-GAN which generates the samples by transforming a predifined arbitrary input noise for the latent variable $h\sim p(h)$ via $x' = f(h)$, the generative model in variational auto GAN-coder has richer structure, $p(x') = \int p(x'|h)p(h|x)\hat p(x)dhdx$. This proposed model takes the data distribution to discover the latent variable, and then, generates the data based on the corresponding latent variable. 

\subsection{Learning variational auto $f$-GAN-coder}

We can learn the $p(h|x)$ and $p(x'|h)$ by optimizing~\eq{eq:VAE-GAN} via stochastic gradient descent for saddle-point problem. Comparing to the original objective of variational auto-encoder~\eq{eq:VAE_obj}, which is only applicable to the $KL$-divergence, the objective~\eq{eq:VAE-GAN} is valid for all the $f$-divergence with arbitrary $\phi$.

We denote the parameters in $v(\cdot)$ as $W$, and apply the reparametrization trick to $p(h|x)$, $p(x|h)$, which results $h = \alpha_U(x, \epsilon)$ and $x = \beta_V(h, \xi)$ where $\epsilon, \xi$ are sampled from some distributions. Then, we apply the stochastic gradient descent to the saddle point. 

\begin{algorithm}[H]
  \caption{{SGD for variational auto $f$-GAN-coder} }
    \begin{algorithmic}[1]\label{alg:SGD_VAF}
      \STATE Initialize $U, V, W$ randomly as $U_0, V_0, W_0$.
      \FOR{$i=1,\ldots, t$}
	    \STATE Sample $x_i$ from training data.
    	\STATE Sample $\epsilon_i\,, \xi_i$.
    	\STATE Obtain $h_i = \alpha_{U_t}(x_i, \epsilon_i)$, $x'_i= \beta_{V_t}(x'|h_i)$.
    	\STATE Compute stochastic gradients $\nabla_U, \nabla_V, \nabla_W$ w.r.t. $U,V,W$.
    	\STATE Update the parameters $U_t = U_{t-1}-\gamma_t\nabla_U$, $V_t = V_{t-1}-\gamma_t\nabla_V$, and $W_t = W_{t-1} + \gamma_t \nabla_W$.
      \ENDFOR\\
    \end{algorithmic}
\end{algorithm}
%
We specify the gradients computation procedures, given reparametrization $h = \alpha_U(x, \epsilon)$ and $x' = \beta_V(h, \xi)$ for $p(h|x)$ and $p(x|h)$, respectively. With the reparametrization, we can re-write the objective as 
\begin{eqnarray}
\min_{U, V}\max_{W} L(U, V, W) := \EE_{x\sim \hat p(x), \epsilon, \xi}\sbr{v_W(\beta_V(\alpha_U(x, \epsilon), \xi)) - \phi^*(v_W(x))}.
\end{eqnarray}

The stochastic gradient can be computed by using Back-Propagation as 
\begin{itemize}
\item[1.] $\nabla_W L = \nabla_W v_W(x')-\phi^*(v_W(x))\nabla_W v_W(x)$.
\item[2.] $\nabla_V L = \nabla_V v_W(x')\nabla_V \beta_V(\alpha_U(x, \epsilon), \xi)$
\item[3.] $\nabla_U L = \nabla_U v_W(x')\nabla_{y=\alpha_U(x, \epsilon)} \beta_V(y, \xi)\nabla_U \alpha_U(x, \epsilon)$
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Reference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{unsrt}
\bibliography{../../bibfile/bibfile}


\end{document}
